from flask import Flask, Response, json
# import json

app = Flask(__name__)
@app.route('/svc-2')
def cluster_properties2():
    newDict={}
    with open ('lab1-las.properties', 'r') as filephani:
        for chidduline in filephani:
            if  not chidduline.startswith('#') and len(chidduline) > 1:
                split_var = chidduline.split("=")
                newDict[split_var[0]] = split_var[1]
                # response = app.response_class(
                #     response=json.dump(newDict),
                #     status=200,
                #     mimetype='application/json'
                # )
# parsed = json.loads(str(newDict)) #### json.loads converts string to dictionary
# json.dumps = converts dictionary to string
    # return json.dumps(newDict, indent=5, sort_keys=True)
    return Response(response=json.dumps(newDict), mimetype="application/json")
    # return response
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=int("8080"), debug=True)
